%define gobuild(o:) go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '-Wl,-z,relro -Wl,-z,now -specs=/usr/lib/rpm/generic-hardened-ld '" -a -v -x %{?**};

Name:          podman
Version:       4.9.4
Release:       8
Summary:       A tool for managing OCI containers and pods.
Epoch:         1
License:       Apache-2.0 and MIT
URL:           https://podman.io/
Source0:       https://github.com/containers/podman/archive/refs/tags/v%{version}.tar.gz
Source1:       https://github.com/containers/dnsname/archive/18822f9a4fb35d1349eb256f4cd2bfd372474d84/dnsname-18822f9.tar.gz
Source2:       https://github.com/containers/gvisor-tap-vsock/archive/refs/tags/v0.7.1.tar.gz
Source3:       https://github.com/cpuguy83/go-md2man/archive/refs/tags/v2.0.3.tar.gz
Patch0:        0001-podman-4.9.4-add-support-for-loongarch64.patch

Patch0001:    0001-fix-CVE-2024-28180.patch
Patch0002:    0002-fix-CVE-2023-3978.patch
Patch0003:    0003-fix-CVE-2023-48795.patch
Patch0004:    0004-fix-CVE-2022-3064.patch

BuildRequires: gcc golang btrfs-progs-devel glib2-devel glibc-devel glibc-static
BuildRequires: gpgme-devel libassuan-devel libgpg-error-devel libseccomp-devel libselinux-devel
BuildRequires: ostree-devel pkgconfig make git-core systemd systemd-devel shadow-subid-devel man-db
Requires:      crun containers-common containernetworking-plugins iptables nftables conmon container-selinux catatonit
Provides:      %{name}-quadlet
Obsoletes:     %{name}-quadlet <= 5:4.4.0-1
Provides:      %{name}-quadlet = %{epoch}:%{version}-%{release}

%description
Podman manages the entire container ecosystem which includes pods,
containers, container images, and container volumes using the libpod library. 

%package        docker
Summary:        Docker CLI emulator for podman
BuildArch:      noarch
Requires:       %{name} = %{epoch}:%{version}-%{release}
Conflicts:      docker docker-latest docker-ce docker-ee moby-engine

%description docker
This package installs a script named docker, which emulates the Docker CLI through podman command.

%package tests
Summary: Tests for %{name}

Requires: %{name} = %{epoch}:%{version}-%{release}
Requires: bats
Requires: jq
Requires: skopeo
Requires: nmap-ncat
Requires: httpd-tools
Requires: openssl
Requires: socat
Requires: buildah
Requires: gnupg

%description tests
%{summary}

This package contains system tests for %{name}

%package remote
Summary: (Experimental) Remote client for managing %{name} containers

%description remote
Remote client for managing %{name} containers.

This experimental remote client is under heavy development. Please do not
run %{name}-remote in production.

%{name}-remote uses the version 2 API to connect to a %{name} client to
manage pods, containers and container images. %{name}-remote supports ssh
connections as well.

%package plugins
Summary: Plugins for %{name}
Requires: dnsmasq
Recommends: %{name}-gvproxy = %{epoch}:%{version}-%{release}

%description plugins
This plugin sets up the use of dnsmasq on a given CNI network so
that Pods can resolve each other by name.  When configured,
the pod and its IP address are added to a network specific hosts file
that dnsmasq will read in.  Similarly, when a pod
is removed from the network, it will remove the entry from the hosts
file.  Each CNI network will have its own dnsmasq instance.

%package gvproxy
Summary: Go replacement for libslirp and VPNKit

%description gvproxy
A replacement for libslirp and VPNKit, written in pure Go.
It is based on the network stack of gVisor. Compared to libslirp,
gvisor-tap-vsock brings a configurable DNS server and
dynamic port forwarding.

%package help
Summary:        Help document for the podman package
Conflicts:      docker docker-latest docker-ce docker-ee moby-engine

%description help
Help document for the podman package

%package -n %{name}sh
Summary: Confined login and user shell using %{name}
Requires: %{name} = %{epoch}:%{version}-%{release}
Provides: %{name}-shell = %{epoch}:%{version}-%{release}
Provides: %{name}-%{name}sh = %{epoch}:%{version}-%{release}

%description -n %{name}sh
%{name}sh provides a confined login and user shell with access to volumes and
capabilities specified in user quadlets.

It is a symlink to %{_bindir}/%{name} and execs into the `%{name}sh` container
when `%{_bindir}/%{name}sh` is set as a login shell or set as os.Args[0].

%prep
%setup -n %{name}-%{version}
sed -i 's;@@PODMAN@@\;$(BINDIR);@@PODMAN@@\;%{_bindir};' Makefile
%patch0001 -p1
# untar dnsname
tar zxf %{SOURCE1}
%patch0002 -p1
%patch0004 -p1
# untar %%{name}-gvproxy
tar zxf %{SOURCE2}
%patch0003 -p1
# untar go-md2man
tar zxf %{SOURCE3}
%ifarch loongarch64
cd dnsname-18822f9a4fb35d1349eb256f4cd2bfd372474d84
export GOSUMDB="sum.golang.org"
export GOPROXY="https://goproxy.cn"
go get -d golang.org/x/sys@v0.19.0
go mod tidy
go mod download
go mod vendor
cd -
%patch0 -p1
%endif


%build
GO_MD2MAN_PATH="$(pwd)%{_bindir}"
mkdir -p _buildgo2man/bin $GO_MD2MAN_PATH
cd go-md2man-*
go build -mod=vendor -o ../_buildgo2man/bin/go-md2man .
cp ../_buildgo2man/bin/go-md2man $GO_MD2MAN_PATH/go-md2man
export PATH=$GO_MD2MAN_PATH:$PATH
cd -

%set_build_flags
export GO111MODULE=on
export CGO_CFLAGS=$CFLAGS
export CGO_CFLAGS="$CGO_CFLAGS -Wno-unused-function"

%ifarch x86_64
export CGO_CFLAGS="$CGO_CFLAGS -m64 -mtune=generic -fcf-protection=full"
%endif

LDFLAGS="-X %{ld_libpod}/define.buildInfo=$(date +%s) \
         -X %{ld_libpod}/config._installPrefix=%{_prefix} \
         -X %{ld_libpod}/config._etcDir=%{_sysconfdir} \
         -X %{ld_project}/pkg/systemd/quadlet._binDir=%{_bindir}"

# build rootlessport first
%gobuild -o bin/rootlessport ./cmd/rootlessport

export BASEBUILDTAGS="seccomp exclude_graphdriver_devicemapper $(hack/systemd_tag.sh) $(hack/libsubid_tag.sh)"

# build %%{name}
export BUILDTAGS="$BASEBUILDTAGS $(hack/btrfs_installed_tag.sh) $(hack/btrfs_tag.sh) $(hack/libdm_tag.sh)"
%gobuild -o bin/%{name} ./cmd/%{name}

# build %%{name}-remote
export BUILDTAGS="$BASEBUILDTAGS exclude_graphdriver_btrfs btrfs_noversion remote"
%gobuild -o bin/%{name}-remote ./cmd/%{name}

# build quadlet
export BUILDTAGS="$BASEBUILDTAGS $(hack/btrfs_installed_tag.sh) $(hack/btrfs_tag.sh)"
%gobuild -o bin/quadlet ./cmd/quadlet

pushd dnsname-18822f9a4fb35d1349eb256f4cd2bfd372474d84
mkdir _build
pushd _build
mkdir -p src/github.com/containers
ln -s ../../../../ src/github.com/containers/dnsname
popd
ln -s vendor src
export GO111MODULE=off
export GOPATH=$(pwd)/_build:$(pwd)
%gobuild -o bin/dnsname github.com/containers/dnsname/plugins/meta/dnsname
popd

pushd gvisor-tap-vsock-0.7.1
export GO111MODULE=on
export GOFLAGS=-mod=vendor
export GOPATH=$(pwd)/_build:$(pwd)
%gobuild -o bin/gvproxy ./cmd/gvproxy
%gobuild -o bin/gvforwarder ./cmd/vm
popd

make docs docker-docs

%install
install -dp %{buildroot}%{_unitdir}
PODMAN_VERSION=%{version} %{__make} PREFIX=%{buildroot}%{_prefix} ETCDIR=%{_sysconfdir} \
        install.bin \
        install.man \
        install.systemd \
        install.completions \
        install.docker \
        install.docker-docs \
        install.remote \
        install.modules-load

sed -i 's;%{buildroot};;g' %{buildroot}%{_bindir}/docker

# install dnsname plugin
pushd dnsname-18822f9a4fb35d1349eb256f4cd2bfd372474d84
%{__make} PREFIX=%{_prefix} DESTDIR=%{buildroot} install
popd

# install gvproxy
pushd gvisor-tap-vsock-0.7.1
install -dp %{buildroot}%{_libexecdir}/%{name}
install -p -m0755 bin/gvproxy %{buildroot}%{_libexecdir}/%{name}
install -p -m0755 bin/gvforwarder %{buildroot}%{_libexecdir}/%{name}
popd

# do not include docker and podman-remote man pages in main package
for file in `find %{buildroot}%{_mandir}/man[15] -type f | sed "s,%{buildroot},," | grep -v -e remote -e docker`; do
    echo "$file*" >> podman.file-list
done

rm -f %{buildroot}%{_mandir}/man5/docker*.5

install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav test/system %{buildroot}/%{_datadir}/%{name}/test/

%global license %doc

%files
%license LICENSE
%doc README.md CONTRIBUTING.md install.md transfer.md
%{_bindir}/%{name}
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/rootlessport
%{_libexecdir}/%{name}/quadlet
%{_datadir}/bash-completion/completions/%{name}
# By "owning" the site-functions dir, we don't need to Require zsh
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%{_unitdir}/%{name}*
%{_userunitdir}/%{name}*
%{_tmpfilesdir}/%{name}.conf
%{_systemdgeneratordir}/%{name}-system-generator
%{_systemdusergeneratordir}/%{name}-user-generator
%{_modulesloaddir}/%{name}-iptables.conf

%files docker
%{_bindir}/docker
%{_tmpfilesdir}/%{name}-docker.conf
%{_prefix}/share/user-tmpfiles.d/%{name}-docker.conf

%files help
%{_mandir}/man1/{docker*.1*,podman*.1*}
%{_mandir}/man5/*.5*

%files remote
%license LICENSE
%{_bindir}/%{name}-remote
%{_mandir}/man1/%{name}-remote*.*
%{_datadir}/bash-completion/completions/%{name}-remote
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}-remote.fish
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}-remote

%files tests
%license LICENSE
%{_datadir}/%{name}/test

%files plugins
%license dnsname-18822f9a4fb35d1349eb256f4cd2bfd372474d84/LICENSE
%doc dnsname-18822f9a4fb35d1349eb256f4cd2bfd372474d84/{README.md,README_PODMAN.md}
%dir %{_libexecdir}/cni
%{_libexecdir}/cni/dnsname

%files gvproxy
%license gvisor-tap-vsock-0.7.1/LICENSE
%doc gvisor-tap-vsock-0.7.1/README.md
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/gvproxy
%{_libexecdir}/%{name}/gvforwarder

%files -n %{name}sh
%{_bindir}/%{name}sh

%changelog
* Tue Apr 30 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:4.9.4-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2022-3064

* Mon Apr 29 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:4.9.4-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-48795

* Sun Apr 28 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:4.9.4-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-3978

* Wed Apr 24 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:4.9.4-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-28180

* Wed Apr 24 2024 Pengda Dou <doupengda@loongson.cn> - 1:4.9.4-4
- add support for loongarch64

* Mon Apr 08 2024 lijian <lijian2@kylinos.cn> 1:4.9.4-3
- Add require containernetworking-plugins

* Wed Mar 27 2024 lijian <lijian2@kylinos.cn> 1:4.9.4-2
- delete unused source podman-machine-cni

* Tue Mar 26 2024 lijian <lijian2@kylinos.cn> 1:4.9.4-1
- update to 4.9.4
- Fixed CVE-2024-1753 in Buildah and podman build
- Fixed a bug in health check

* Sun Feb 25 2024 lijian <lijian2@kylinos.cn> 1:4.9.3-1
- update to 4.9.3
- delete plugin podman-machine-cni

* Mon Feb 19 2024 lijian <lijian2@kylinos.cn> 1:4.5.1-2
- Resolve compilation failure with issue I9248E

* Fri Jun 30 2023 lijian <lijian2@kylinos.cn> 1:4.5.1-1
- update to 4.5.1

* Tue Feb 14 2023 fushanqing <fushanqing@kylinos.cn> - 1:3.4.4-3
- fix obs build problem

* Tue Jul 26 2022 laokz <laokz@foxmail.com> - 1:3.4.4-2
- remove redundant patch2

* Tue Jul 05 2022 fushanqing <fushanqing@kylinos.cn> - 1:3.4.4-1
- update to 3.4.4

* Thu Apr 14 2022 wangkai<wangkai385@h-partners.com> - 0.10.1-12
- Fix build error

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.10.1-11
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Wed Mar 31 2021 maminjie <maminjie1@huawei.com> - 1:0.10.1-10
- Eat signal 23 in signal proxy
- Require container-selinux only when selinux-policy is installed

* Sat Mar 13 2021 maminjie <maminjie1@huawei.com> - 1:0.10.1-9
- Fix the invalid memory address reference

* Wed Mar 3 2021 wangxiao <wangxiao65@huawei.com> - 1:0.10.1-8
- Fix CVE-2021-20188

* Thu Feb 18 2021 lingsheng <lingsheng@huawei.com> - 1:0.10.1-7
- Resolve go-md2man dependency

* Mon Feb 8 2021 lingsheng <lingsheng@huawei.com> - 1:0.10.1-6
- Change BuildRequires to golang

* Sat Jan 9 2021 Shengjing Wei <weishengjing1@huawei.com> - 1:0.10.1-5
- Fixed podman pull failed with issue I2BF99

* Wed Sep 9 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1:0.10.1-4
- Add conflicts with docker-engine for help package

* Thu Mar 12 2020 Ling Yang <lingyang2@huawei.com> - 1:0.10.1-3
- Fixed install fail

* Mon Dec 2 2019 shijian <shijian16@huawei.com> - 1:0.10.1-2
- Package init
